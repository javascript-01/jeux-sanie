"use strict";
// function main
(() => {
  const grilles = new Grille("grid", 16000, 50);

  grilles.start();

  window.addEventListener("keydown", (event) => {
    if (event.keyCode >= 37 && event.keyCode <= 40) {
      grilles.changeDirection(event.keyCode);
    }
  });
})();















