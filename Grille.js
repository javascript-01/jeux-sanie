class Grille {
    constructor(grille, nombreCase, vitesse) {
      this.grid = document.getElementById(grille);
      this.vitesse = vitesse;
  
  
      // créer la grille
      for (let compteur = 0; compteur < nombreCase; compteur++) {
        const y = parseInt(compteur / 160 + 1);
        const x = (compteur % 160) + 1;
        const nombre = compteur + 1;
        const cases = document.createElement("div");
        cases.setAttribute("id", nombre);
        cases.setAttribute("y", y);
        cases.setAttribute("x", x);
        grid.appendChild(cases);
      }
  
      //créer et positione le snake
      const snake = document.getElementById("6500");
      snake.classList.add("in");
      this.snake = [6500];
  
      //direction par defaut
      this.direction = "n";
  
      this.tailleGrille = nombreCase;
  
      // créer l'apat
      this.afficherAppat();
    }
  
    // change la direction du snake
    changeDirection(code) {
      switch (code) {
        case 37:
          if (this.direction != "e") this.direction = "o";
          break;
        case 38:
          if (this.direction != "s") this.direction = "n";
          break;
        case 39:
          if (this.direction != "o") this.direction = "e";
          break;
        case 40:
          if (this.direction != "n") this.direction = "s";
          break;
      }
    }
  
    init() {
      // supprime l'ancien snake
  
      for (let compteur = 0; compteur < this.snake.length; compteur++) {
        const nombre = this.snake[compteur];
        const elementOldSnake = document.getElementById(nombre);
        elementOldSnake.classList.remove("in");
      }
  
      // on replace le snake à sa position de départ
  
      const snake = document.getElementById("6500");
      snake.classList.add("in");
      this.snake = [6500];
    }
  
    // genere et affiche un appat
    afficherAppat() {
      const nombre = Math.floor(Math.random() * (this.tailleGrille - 1 + 1)) + 1;
  
      const testTouche = this.snake.indexOf(nombre);
  
      if (testTouche === -1) {
        this.deleteAppat();
        this.appat = nombre;
        const cases = document.getElementById(nombre);
        cases.classList.add("appat");
      } else {
        this.afficherAppat();
      }
    }
  
    deleteAppat() {
      const appats = document.getElementsByClassName("appat");
  
      for (let compteur = 0; compteur < appats.length; compteur++) {
        const appat = appats[compteur];
        console.log(appat);
        appat.classList.remove("appat");
      }
    }
  
    // démare l'animation
    start() {
      this.interval = setInterval(() => {
        this.loop();
      }, this.vitesse);
    }
  
    // stop l'animation
    pause() {
      clearInterval(this.interval);
    }
  
    // recommencer
    reset() {}
  
    // si perdu
    gameOver() {
      let reload = confirm("voulez vous rééssayer");
      if (reload) {
        this.init();
      } else {
        clearInterval(this.interval);
      }
    }
  
    loop() {
      const caseNext = this.getNextCase();
      const testAppat = this.TestColisionApat(caseNext);
      if (testAppat) {
        this.afficherAppat();
      }
      const resutlColision = this.TestColisionSnake(caseNext);
  
      if (caseNext != false && resutlColision != true)
        this.moveSneaker(caseNext, testAppat);
    }
    // calcule la case suivante
    getNextCase() {
      const tete = this.snake[0];
      const teteElement = document.getElementById(tete);
      let y = teteElement.getAttribute("y");
      let x = teteElement.getAttribute("x");
      switch (this.direction) {
        case "n":
          y--;
          break;
        case "s":
          y++;
          break;
        case "o":
          x--;
          break;
        case "e":
          x++;
          break;
      }
  
      if (y === 0 || y === 101 || x === 0 || x === 161) {
        this.gameOver();
        return false;
      }
  
      let nombre = (y - 1) * 160 + parseInt(x);
  
      return nombre;
    }
  
    // fait avancer le snaker d'une case et retire la derniere case
    moveSneaker(nombre, toucheAppat) {
      this.snake.unshift(nombre);
      const newHeadSnake = document.getElementById(nombre);
      newHeadSnake.classList.toggle("in");
  
      if (!toucheAppat) {
        const lastElement = this.snake[this.snake.length - 1];
        const DeleteQueueSnake = document.getElementById(lastElement);
        DeleteQueueSnake.classList.toggle("in");
        this.snake.pop();
      }
    }
  
    // verifie si il y as une colision avec l'appat
    TestColisionApat(caseNext) {
      // on verifie si la case suivante touche l'apat
  
      const testToucheAppat = this.appat === caseNext;
  
      if (testToucheAppat == false) {
        return false;
      } else {
        return true;
      }
    }
  
    //verifie si le snake entre en collision avec lui même
    TestColisionSnake(caseNext) {
      const testToucheSnake = this.snake.indexOf(caseNext);
  
      if (testToucheSnake === -1) {
        return false;
      } else {
        this.gameOver();
        return true;
      }
    }
  }